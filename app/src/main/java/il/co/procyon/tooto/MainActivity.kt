package il.co.procyon.tooto

import android.os.Bundle

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

import androidx.appcompat.app.AppCompatActivity

import java.util.ArrayList

import il.co.procyon.tootolib.Focusable
import il.co.procyon.tootolib.FocusableView
import il.co.procyon.tootolib.TutorialFrag

class MainActivity : AppCompatActivity() {
    private val TAG = this.javaClass.simpleName
    private val mViews: ArrayList<FocusableView>? = null


    @Focusable(comment = "This is a button 0")
    private var mBtn1: Button? = null

    @Focusable(comment = "Notice the hello world 2")
    private var mHellowWorld: TextView? = null

    @Focusable(comment = "Nice kitty! 1")
    private var mImgCat: ImageView? = null

    @Focusable(comment = "Bla Bla 4")
    private var mYadayada: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mBtn1 = findViewById(R.id.btn1)
        mHellowWorld = findViewById(R.id.textView)
        mImgCat = findViewById(R.id.img_cat)
        mYadayada = findViewById(R.id.dummy_text)

        //        mViews = new ArrayList<>();
        //        mViews.add(new FocusableView(mBtn1, "This is a button 0", 0, FocusableView.FocusableType.ROUND));
        //        mViews.add(new FocusableView(mHellowWorld, "Notice the hello world 2", 2, FocusableView.FocusableType.ROUND));
        //        mViews.add(new FocusableView(mImgCat, "Nice kitty! 1", 1, FocusableView.FocusableType.ROUND));
        //        mViews.add(new FocusableView(mYadayada, "Bla Bla 4", 4, FocusableView.FocusableType.ROUND));


        mBtn1!!.setOnClickListener { openTootoFrag() }
    }


    fun openTootoFrag() {

        TutorialFrag.Builder().setAnnotatedHolderObj(this).show(supportFragmentManager)

    }
}
