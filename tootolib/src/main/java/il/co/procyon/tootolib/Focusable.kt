package il.co.procyon.tootolib

import java.lang.annotation.RetentionPolicy


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD)
annotation class Focusable(val order: Int = 0, val comment: String, val type: FocusableView.FocusableType = FocusableView.FocusableType.ROUND)
