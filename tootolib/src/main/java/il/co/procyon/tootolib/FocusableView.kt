package il.co.procyon.tootolib

import android.view.View

import java.lang.ref.WeakReference

/**
 * Created by Yaadm on 25/9/2017.
 */

class FocusableView(view: View, val viewDescriptio: String, val displayOrder: Int, val focusableType: FocusableType) : Comparable<FocusableView> {


    private val mViewWR: WeakReference<View>

    val view: View?
        get() = mViewWR.get()


    enum class FocusableType {
        ROUND, RECTANGULAR
    }

    init {
        mViewWR = WeakReference(view)
    }

    override fun compareTo(compaingView: FocusableView): Int {
        return displayOrder - compaingView.displayOrder
    }
}
