package il.co.procyon.tootolib;




import android.app.Dialog;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;


/**
 * Created by hanan on 11-Sep-17.
 */

public class TutorialFrag extends DialogFragment implements View.OnClickListener {

    private final String TAG = this.getClass().getSimpleName();

    //    private ArrayList<View> mFocusViews;
    private ArrayList<FocusableView> mFocusViews;
    private ImageView mNext;
    private ImageView mPreviouse;
    private FocusOverlay mFocusOverlay;

    int mCurrentLocation = 0;
    private String[] mDesc;

    public TutorialFrag() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.TootoTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tooto_background, container, false);

        mNext = view.findViewById(R.id.btn_next);
        mPreviouse = view.findViewById(R.id.btn_previous);
//        TextView description = (TextView) view.findViewById(R.id.tv_explanation);
        mFocusOverlay = view.findViewById(R.id.focus_overlay);
        ImageView closeBtn = view.findViewById(R.id.btn_close);

        mNext.setOnClickListener(this);
        mPreviouse.setOnClickListener(this);
        closeBtn.setOnClickListener(this);

        Log.d(TAG, "onCreateView: focusable views=\n" + mFocusViews);


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mFocusViews != null && mFocusViews.size() > 0) {
            setFocusToView(mFocusViews.get(0));
        }
    }

    public void show(FragmentManager fragmentManager) {
        this.show(fragmentManager, null);
    }

    private void onNext() {
        if (mFocusViews != null && mFocusViews.size() > 0) {


            int before = mCurrentLocation;

            mCurrentLocation = (mCurrentLocation + 1) % mFocusViews.size();
            Log.d(TAG, "onNext: current location " + before + " next location=" + mCurrentLocation);
            FocusableView focusView = mFocusViews.get(mCurrentLocation);

            setFocusToView(focusView);
        }
    }

    private void onPrevious() {
        if (mFocusViews != null && mFocusViews.size() > 0) {


            mCurrentLocation = (mCurrentLocation - 1 + mFocusViews.size()) % mFocusViews.size();

            FocusableView focusView = mFocusViews.get(mCurrentLocation);

            setFocusToView(focusView);
        }
    }

    private void setFocusToView(FocusableView view) {
        int location[] = new int[2];
        view.getView().getLocationOnScreen(location);
        Log.d(TAG, "onViewCreated: " + location[0] + ", " + location[1]);
        RectF rect = new RectF(location[0], location[1], location[0] + view.getView().getWidth(), location[1] + view.getView().getHeight());
//            focusView.getDrawingRect(rect);
//            focusView.getLocalVisibleRect(rect);
//        mFocusOverlay.setRect(rect, view.getViewDescriptio());
        mFocusOverlay.setRect(view);
    }


    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btn_next) {
            onNext();
        } else if (i == R.id.btn_previous) {
            onPrevious();
        } else if (i == R.id.btn_close) {
            this.dismiss();
        }
    }

    public interface PopulateWithView {
        Build setViews(ArrayList<FocusableView> focusableViews);
        Build setAnnotatedHolderObj(Object holder);
    }

    public interface Build {
        void show(FragmentManager fragmentManager);

    }


    public static class Builder implements PopulateWithView, Build {


        private TutorialFrag mTutorialFrag;

        public Builder() {
            mTutorialFrag = new TutorialFrag();
        }


        @Override
        public Build setViews(ArrayList<FocusableView> focusableViews) {
            Collections.sort(focusableViews);
            mTutorialFrag.mFocusViews = focusableViews;
            return this;
        }

        @Override
        public Build setAnnotatedHolderObj(Object holder) {
            ArrayList<FocusableView> focusableViews = new ArrayList<>();
            Field[] holderFields = holder.getClass().getDeclaredFields();
            for (Field holderField : holderFields) {
                if (holderField.isAnnotationPresent(Focusable.class)) {
                    try {
                        holderField.setAccessible(true);
                        Focusable annotation = holderField.getAnnotation(Focusable.class);
                        int order = annotation.order();
                        String comment = annotation.comment();
                        FocusableView.FocusableType type = annotation.type();
                        Log.d("TutorialFrag.Builder", "setAnnotatedHolder: field type = " + holderField.getType().getSimpleName());

                        View view = (View) holderField.get(holder);

                        FocusableView focusableView = new FocusableView(view, comment, order, type);
                        focusableViews.add(focusableView);

                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
            Collections.sort(focusableViews);
            mTutorialFrag.mFocusViews = focusableViews;
            return this;
        }


        @Override
        public void show(FragmentManager fragmentManager) {
            mTutorialFrag.show(fragmentManager);

        }

    }
}
