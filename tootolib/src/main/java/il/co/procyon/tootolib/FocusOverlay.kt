package il.co.procyon.tootolib

import android.animation.Animator
import android.animation.ValueAnimator
import android.annotation.TargetApi
import android.content.Context
import android.graphics.BlurMaskFilter
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.graphics.RadialGradient
import android.graphics.RectF
import android.opengl.ETC1.getHeight
import android.os.Build
import androidx.core.content.ContextCompat
import android.text.TextPaint
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.animation.DecelerateInterpolator


/**
 * Created by hanan on 11-Sep-17.
 */

class FocusOverlay @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {

    private val TAG = this::class.simpleName

    var mBackgroundPaint: Paint
    private var PADDING = 4

    var mContext: Context
    private var mWidth: Int = 0
    private var mHeight: Int = 0
    private var mTransparentFill: Paint? = null

    private var mCurrentRect: RectF? = null
    private var mPreviouseRect: RectF? = null
    private var mBoundsPaint: Paint? = null
    private var mDescription: String? = null
    private var mTextPaint: Paint? = null
    private val mShader: RadialGradient? = null

    private var mGrowAnimator: ValueAnimator? = null
    private var mGrowFactor: Float = 0.toFloat()
    private var mYPadding: Int = 0


    init {
        mContext = context

        PADDING = (context.resources.displayMetrics.density * PADDING + 0.5).toInt()

        mBackgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = ContextCompat.getColor(context, R.color.semi_transparent_background)
            style = Paint.Style.FILL
        }

        mTransparentFill = Paint(mBackgroundPaint).apply {
            xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
            maskFilter = BlurMaskFilter(15f, BlurMaskFilter.Blur.NORMAL)
        }

        mWidth = (context.resources.displayMetrics.density * 200 + 0.5).toInt()
        mHeight = (context.resources.displayMetrics.density * 50 + 0.5).toInt()

        mBoundsPaint = Paint().apply {
            color = Color.GREEN
            style = Paint.Style.STROKE
            strokeWidth = 3.0f
        }

        mTextPaint = TextPaint(Paint.ANTI_ALIAS_FLAG).apply {
            textAlign = Paint.Align.CENTER
            textSize = 80.0f
            color = Color.WHITE
        }


        mGrowAnimator = ValueAnimator.ofFloat(0.01f, 1.0f).apply {
            interpolator = DecelerateInterpolator()
            duration = 400
        }


    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawPaint(mBackgroundPaint)
        Log.d(TAG, "onDraw: view height= $measuredHeight")

        if (mCurrentRect != null) {
            Log.d(TAG, """onDraw: rect corner left=${mCurrentRect!!.left}, top=${mCurrentRect!!.top}""")

            val halfWidth = mCurrentRect!!.centerX() - mCurrentRect!!.left
            val halfHeight = mCurrentRect!!.centerY() - mCurrentRect!!.top
            val absoluteRadius = Math.pow(Math.pow(halfHeight.toDouble(), 2.0) + Math.pow(halfWidth.toDouble(), 2.0), 0.5).toFloat() + PADDING
            val r = absoluteRadius * mGrowFactor

            //            mShader = new RadialGradient(
            //                    mCurrentRect.centerX(),
            //                    mCurrentRect.centerY(),
            //                    r,
            //                    new int[]{ContextCompat.getColor(mContext, R.color.semi_transparent_background), Color.TRANSPARENT},
            //                    new float[]{0.9f, 1.0f},
            //                    Shader.TileMode.CLAMP);
            //
            //            mTransparentFill.setShader(mShader);

            //grow new circle;
            canvas.drawCircle(mCurrentRect!!.centerX(), mCurrentRect!!.centerY() - mYPadding, r, mTransparentFill!!)

            //shrink previous circle:
            if (mPreviouseRect != null) {
                val halfWidthPrev = mPreviouseRect!!.centerX() - mPreviouseRect!!.left
                val halfHeightPrev = mPreviouseRect!!.centerY() - mPreviouseRect!!.top
                val absoluteRadiusPrev = Math.pow(Math.pow(halfHeightPrev.toDouble(), 2.0) + Math.pow(halfWidthPrev.toDouble(), 2.0), 0.5).toFloat() + PADDING
                val inversR = absoluteRadiusPrev * (1.0f - mGrowFactor)
                canvas.drawCircle(mPreviouseRect!!.centerX(), mPreviouseRect!!.centerY() - mYPadding, inversR, mTransparentFill!!)
            }


            //            canvas.drawRect(mCurrentRect.left, mCurrentRect.top - 65, mCurrentRect.right, mCurrentRect.bottom - 65, mTransparentFill);

            val height: Int
            if (mCurrentRect!!.top > getHeight() / 2) {
                height = 400
            } else {
                height = getHeight() - 400
            }

            canvas.drawText(mDescription!!, (width / 2).toFloat(), height.toFloat(), mTextPaint!!)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        Log.d(TAG, "onSizeChanged: new height= $h, old height= $oldh")

        val location = IntArray(2)
        getLocationOnScreen(location)

        mYPadding = location[1]

        Log.d(TAG, "onSizeChanged: location= " + location[0] + ", " + location[1])

    }

    //    @Override
    //    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    //        int absoluteHeight = MeasureSpec.makeMeasureSpec(heightMeasureSpec, MeasureSpec.AT_MOST);
    //
    //        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
    //        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
    //        Log.d(TAG, "onMeasure: parent height= " + parentHeight + ",  heightMeasuredSpec= " + heightMeasureSpec + ", make exact spec= " + absoluteHeight);
    //        this.setMeasuredDimension(parentWidth, parentHeight);
    //        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    //    }

    fun setRect(focusableView: FocusableView) {
        focusableView.view?.let {
            val location = IntArray(2)
            it.getLocationOnScreen(location)
            Log.d(TAG, "onViewCreated: " + location[0] + ", " + location[1])
            val rect = RectF(location[0].toFloat(), location[1].toFloat(), (location[0] + it.width).toFloat(), (location[1] + it.height).toFloat())

            setRect(rect, focusableView.viewDescriptio)
        }


    }

    fun setRect(rect: RectF, description: String) {

        mPreviouseRect = mCurrentRect
        mCurrentRect = rect

        mDescription = description

        mGrowAnimator!!.addUpdateListener { valueAnimator ->
            mGrowFactor = valueAnimator.animatedValue as Float
            this@FocusOverlay.invalidate()
        }

        mGrowAnimator!!.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animator: Animator) {

            }

            override fun onAnimationEnd(animator: Animator) {
                mPreviouseRect = null
            }

            override fun onAnimationCancel(animator: Animator) {

            }

            override fun onAnimationRepeat(animator: Animator) {

            }
        })
        mGrowAnimator!!.start()
        invalidate()
    }
}
